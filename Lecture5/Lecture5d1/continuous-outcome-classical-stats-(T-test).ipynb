{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"text-align: right\">INFO 6105 Data Science Eng Methods and Tools, Lecture 5 Day 1</div>\n",
    "<div style=\"text-align: right\">Dino Konstantopoulos, 30 September 2019, with material from Peter Norvig</div>\n",
    "\n",
    "# Classical Statistical Analysis\n",
    "\n",
    "A **Bayesian model** is described by a model, its parameters, ***and uncertainty in those parameters***. The model is described as the model probability distribution, but its parameters and the uncertainly in its parameters is ***also described as probability distributions***. How strange is that?  \n",
    "\n",
    "**Classical** (also known as **frequentist**) statistical analysis works otherwise. Although showing sings of old age, it's still practiced quite a bit, especially in big pharma, so you need to know about it. Last week, we showed how to build a model (as a pdf), and deduce its parameters via MOM and MLE methods. Today, we'll talk about the **T-test** and the **p-ratio**.\n",
    "\n",
    "Run the cell below, we'll use it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pylab as plt\n",
    "import seaborn as sns\n",
    "sns.set_context('notebook')\n",
    "\n",
    "RANDOM_SEED = 20090425"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Bayesian vs Frequentist Statistics: *What's the difference?*\n",
    "\n",
    "*Any* statistical inferece paradigm, Bayesian or otherwise, involves at least the following: \n",
    "\n",
    "1. Some **unknown quantities**, which we are interested in learning or predicting. These are called the **dependent variables**\n",
    "2. Some **data** which have been observed, and hopefully contains information leading to the dependent variables. These are called the **independent variables**. Note that some of these may be **correlated** (linearly or not), so we should be able to throw the correlated ones and only use the ***really independent variables*** for predicting the dependent ones\n",
    "3. One (or more) **models** that relate the independent variables to the dependent variables via a probablity distribution function (pdf). The pdf will yield variates that essentialy statistically ***look like the real data***. \n",
    "\n",
    "The model is the instrument you use to **learn** about the underlying process that yields the data. For example, you learn about the real world from the model that your parents build for you then teach you, before you leave home to build your own models. Machines build models to learn, too. They either learn them from the data, or we (humans) can also teach them the model, like parents to them!\n",
    "\n",
    "</br >\n",
    "<center>\n",
    "<img src=\"images/robot-daddy.jpg\" width=\"400\" />\n",
    "</center>\n",
    "\n",
    "In a **frequentist** World view, **data** observed is considered **random**, because it is the realization of random processes and hence will vary each time one goes to observe the system. Model **parameters** are considered **fixed**. A parameter's true value may be as of yet unknown, but it's fixed. \n",
    "\n",
    "- For example, Jesus Christ is a central parameter in the Christian World Model. Christians will say the world order may be random because of human misgivings, but Jesus Christ and his compassion (the parameter) is fixed and steadfast.\n",
    "\n",
    "In a **Bayesian** World view,  data is considered **fixed**. Model parameters may not be completely random, but Bayesians use probability distribtutions to describe their uncertainty in values, and are therefore treated as **random variables**. \n",
    "\n",
    "- For example, some Christians may postulate that world order is predetermined, however Jesus Christ's compassion may vary because.. *sometimes he gets exasperated by his followers*!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bayes' Formula\n",
    "\n",
    "While frequentist statistics uses different estimators for different problems, Bayes formula is the **only estimator** that Bayesians need to obtain estimates of unknown quantities. \n",
    "\n",
    "The equation expresses how our belief about the value of \\\\(\\theta\\\\) (the parameter), as expressed by the **prior distribution** \\\\(P(\\theta)\\\\) is reallocated following the observation of the data \\\\(y\\\\). \n",
    "\n",
    "For **discrete random variables**:\n",
    "\n",
    "<div style=\"font-size: 120%;\">  \n",
    "\\\\[Pr(\\theta\\;|\\;y) = \\frac{Pr(\\theta \\cap y)}{Pr(y)} = \\frac{Pr(y\\;|\\;\\theta)Pr(\\theta)}{\\sum_\\theta Pr(y\\;|\\;\\theta)Pr(\\theta)} \\\\]\n",
    "</div>\n",
    "\n",
    "The denominator is actually the expression in the numerator integrated over all possible discrete model parameters \\\\(\\theta\\\\).\n",
    "\n",
    "For **continuous random variables**, the denominator usually cannot be computed directly:\n",
    "\n",
    "<div style=\"font-size: 120%;\">  \n",
    "\\\\[Pr(\\theta\\;|\\;y) = \\frac{Pr(y\\;|\\;\\theta)Pr(\\theta)}{\\int Pr(y\\;|\\;\\theta)Pr(\\theta) d\\theta}\\\\]\n",
    "</div>\n",
    "\n",
    "The denominator is the expression in the numerator integrated over all possible continuous model parameters \\\\(\\theta\\\\)\n",
    "\n",
    "The **intractability** of the integral in the denominator led to the under-utilization of Bayesian methods by statisticians. But with the advent of computers and clever algorithms like [Metropolis-Hastings](https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm), this has changed.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example\n",
    "\n",
    "We'll use a fictitious example from [Kruschke (2012)](http://www.indiana.edu/~kruschke/articles/KruschkeAJ2012.pdf) concerning the evaluation of a clinical trial for drug evaluation. \n",
    "\n",
    "The trial aims to evaluate the efficacy of a \"smart drug\" that is supposed to increase intelligence by comparing IQ scores of individuals in a treatment arm (those receiving the drug) to those in a control arm (those recieving a placebo). There are 47 individuals and 42 individuals in the treatment (`drug`) and control (`placebo`) arms, respectively, and these are their post-trial IQs. An IQ between 90 and 110 is considered average; over 120, superior. Let's look at the histograms of our data, ***first thing you should always do***.\n",
    "\n",
    "Note that although our IQ data is integer type, our datasets here could easily be real-valued, and so we consider our random variable to be **continuous**.\n",
    "\n",
    "Please plot histograms using `pd.concat([drug, placebo], ignore_index=True)`, and then `.hist('iq', by='group')` on the pandas dataframe."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"display:none;\">\n",
    "trial_data.hist('iq', by='group')\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([<matplotlib.axes._subplots.AxesSubplot object at 0x1a1cec9b38>,\n",
       "       <matplotlib.axes._subplots.AxesSubplot object at 0x1a1d17f898>],\n",
       "      dtype=object)"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAX8AAAEUCAYAAADDdzb+AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjEsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy8QZhcZAAAVfElEQVR4nO3dfbBkdX3n8feHQQNiZGTkQRhgCIIPiPKwGpJlEmtTrrVFNIpuRTaRmFU3EFeW2tpaMTGBpNTCSLZYAi5s3FVXkFgEYjRkN1ncZQOrJJQK4WERxbk4g0YRGEUjDzLf/aPPDT3tHebevn376fd+VZ269/5O9+nv9PTv078+53dOp6qQJLVlj0kXIEkaP8Nfkhpk+EtSgwx/SWqQ4S9JDTL8JalBhv+USPKRJO+ZdB3SWkvyiiTb1vgxzkty+Vo+xqwz/CWpQYb/lEuy56RrkDR/DP8JSXJ8ki8keTjJJ4C9uvZXJNmW5J1J/g74cJI3J7lx4P6V5Hnd7xuSfDrJd5PcnOQ9g7eXxi3JQpJ3JbkzyUNJPpxkryVud06Se7q+cGeS1w2sf1uS/9e3/oSu/eAkVye5P8mWJGcNbHqvJJ/o7veFJC/t2+YLk1yfZHuSO5K8Zk2ehClm+E9AkqcDnwQ+BuwHXAW8vu8mB3XthwP/ahmbvAT4fne/X+kWaRr8EvAq4EjgaODdS9zmHmAzsC/wO8DlSZ4LkOSfA+cBpwPPAl4DPJBkD+DTwK3AIcDPAWcneVXfdn+BXt/aD/g48MkkT0vytO6+fwkcALwDuCLJ80f3z55+hv9knAQ8Dbiwqh6vqj8Gbu5bvwM4t6oeraofPNWGkqyj98ZxblX9fVXdCXx0rQqXVujiqtpaVQ8C7wVOG7xBVV1VVV+vqh1V9Qngy8DLu9VvBX6vqm6unq9U1b3Ay4D9q+p3q+qxqvoq8IfAG/s2/fmq+uOqehz4D/Q+XZ/ULc8Ezu/u+7+AP1uqtnnm/uTJOBi4r3a+qt69fb/fX1WPLHNb+9P7f9za17Z1F7eVxq3/tXgvvdf+TpKcDvxbYFPX9EzgOd3vh9L7ZDDocODgJNv72tYBNyz12FW1o5thtPj4W6tqx0Bth+zuHzNPDP/J+AZwSJL0vQEcxpMv8sFLrX4feMbiH0kO6lt3P/BDYCNwd9d26MgrlobT/1o8DPh6/8okh9Mbsf8c8LmqeiLJLUC6m2ylt8to0FZgS1UdtZzH7nYTbex7/EOT7NH3BnAYT/afJrjbZzI+Ry+wz+r2QZ7Kkx9zl3IrcEyS47oDZuctrqiqJ4BrgPOSPCPJC+jtH5WmwduTbEyyH/CbwCcG1u9Db7BzP0CSXwVe3Lf+Q8C/S3Jiep7XvWH8DfBwNzFi7yTrkrw4ycv67ntiklO7GXNnA48CNwF/Dfw98O+7/vcK4NXAH436Hz/NDP8JqKrHgFOBNwMPAr9IL8B3dfu7gd8FrqO3P3RwJs+/pnew7O/oHUS+kt4LXZq0j9M7sPpVep9sdzqRsTtG9fv0BkTfBI4F/m/f+qvoHSv4OPAwvYkS+3WDnp8HjgO2AN+m90axb9/m/5Re33oIeBNwaneM7TF6Yf/Puvt9EDi9qu4a5T982sUvc5k/Sd4PHFRVzvrRxCRZAN5aVddNuhb9KEf+cyDJC5K8pPtY/HLgLcCfTLouSdPLA77z4cfp7eo5mN5H59+n95FXkpbkbh9JatBuR/5JNtA7iHgk8Bi9A46/VlX3JyngNnonJQG8qapuW84DJ/kxeidqfAN4YojapaWsA54L3FxVM3HQ276gNfKUfWG3I/9uitZLqur67u8P0Dva/pYu/H+8qr630qqSnMzOJ2RIo7S5qmbi+kb2Ba2xJfvCbkf+3WnZ1/c13QScuZJHTrIeWD/QvA7ghhtuYOPGjSvZnLRL27ZtY/PmzdAbRc+Kb4B9QaO1u76wogO+3VlyZwKf6mu+vjuJ4r8D5+3io/bZwLlLbXPjxo1s2rRpJWVIyzFLu0+eAPuC1sySfWGlUz3/APgecHH392FV9Y+AnwFeBPzWLu53IXDEwLJ5hY8tSRqRZY/8k1wAHAW8evF6GFW1tfv53SQfondxph9RVduB/gswkWSpm0qSxmBZI/8k7wNOBF67uFsnybOT7N39vifwBuCWtSpUkjQ6uw3/JMcA76J3AtFnk9yS5E+AFwB/neRW4G+Bx9n1bh9J0hRZzmyfO3jy8qqDXjLacqTptlbnvUjj5rV9pJUpet8s9fyqOpbelSrP71v/01V1XLcY/Jpahr+0AlX14OIJj52b6H2rlDRTvLCbNKRhznvZxQmPntmlsTP8x2zTOdcOdb+F808ZcSUagaXOe9ma5Fn0jgv8FvDugfvs8oRHTZ957q/u9pGG0Hfeyy8udd4LvW+V+sdL3NUTHjUVHPlLK9R33ssp/ee9AI9U1Q+e6rwXT3jUtHDkL62A571oXjjyl1bA8140Lxz5S1KDDH9JapDhL0kNMvwlqUGGvyQ1yPCXpAYZ/pLUIMNfkhpk+EtSgwx/SWqQ4S9JDTL8JalBhr8kNcjwl6QGGf6S1CDDX5IaZPhLUoMMf0lqkOEvSQ0y/CWpQYa/JDXI8JekBhn+ktQgw1+SGmT4S1KDDH9JatBuwz/JhiR/nuRLSW5Lck2S/bt1JyW5NcndSf4yyQFrX7IkabWWM/Iv4Peq6vlVdSxwD3B+kj2Ay4G3V9XRwF8B569dqZKkUdlzdzeoqgeB6/uabgLOBE4EHqmqG7v2S4EF4F8ObiPJemD9QPPGlZcrSRqF3YZ/v260fybwKeAw4N7FdVX17SR7JNmve8PodzZw7mqLlSSNxkoP+P4B8D3g4hXe70LgiIFl8wq3IUkakWWP/JNcABwFvLqqdiT5GnB43/rnADuWGPVTVduB7QPbG7poSdLqLGvkn+R99Pbxv7aqHu2aPw/sneTk7u8zgKtGX6IkadR2O/JPcgzwLuBu4LPdiH1LVb0uyZuAy5LsRe9g7y+vYa3SRCXZAHwMOBJ4DPgy8GtVdX+Sk4DLgL3p+kJVfWtStUq7s5zZPncAS+6jqarPAseOuihpSi1Oe74eIMkH6E17fhu9ac9vrqobk7yb3rTnH5n5Jk2LFc32kVrmtGfNE8NfGoLTnjXrvLaPNBynPWumOfKXVshpz5oHjvylFXDas+aFI39pmZz2rHli+EvL5LRnzRN3+0hSgwx/SWqQ4S9JDTL8JalBhr8kNcjwl6QGGf6S1CDDX5IaZPhLUoMMf0lqkOEvSQ0y/CWpQYa/JDXI8JekBhn+ktQgw1+SGmT4S1KDDH9JapDhL0kNMvwlqUGGvyQ1yPCXpAYZ/pLUIMNfkhpk+EtSgwx/SWqQ4S9JDVpW+Ce5IMmWJJXkxX3tC0nuSnJLt7xq7UqVJI3Knsu83SeB/wjcsMS6N1TV7aMrSZK01pYV/lV1I0CSoR4kyXpg/UDzxqE2JklateWO/J/KFem9K9wI/EZVbV/iNmcD547gsSRJI7DaA76bq+qlwMuAABfv4nYXAkcMLJtX+diSpCGtauRfVVu7n48m+SDwqV3cbjuw0yeCYXchSZOU5ALg9cAm4NjF411JFoBHugXgnVX1F5OoUVqOocM/yT7AnlX1nW63zxuBW0ZWmTSdnPygubCs8E9yEXAqcBBwXZIHgFcDVydZB6wD7gR+fa0KlaaBkx80L5Y72+cs4KwlVh0/2nKkmebkB80Mz/CVRsPJD5opo5jqKTXPyQ+aNY78pVVKsk+SfbvfnfygmWD4SyuQ5KIk2+gdpL0uyR3AgcD1Sf4WuB04Gic/aMq520daASc/aF448pekBhn+ktQgw1+SGmT4S1KDDH9JapDhL0kNMvwlqUGGvyQ1yPCXpAYZ/pLUIMNfkhpk+EtSgwx/SWqQ4S9JDfKSzjNi0znXrvg+C+efsgaVSJoHjvwlqUGGvyQ1yPCXpAYZ/pLUIMNfkhpk+EtSgwx/SWqQ4S9JDTL8JalBhr8kNcjwl6QGGf6S1CDDX5IatNvwT3JBki1JKsmL+9qPTvK5JHd3P49a21IlSaOynJH/J4GfAe4daL8UuKSqjgYuAS4bcW2SpDWy2/Cvqhuramt/W5IDgBOAK7umK4ETkuw/+hIlSaM27Je5HArcV1VPAFTVE0m+3rXfP3jjJOuB9QPNG4d8bEnSKo3rgO/ZwJaB5YYxPbY0Mh4D07wYNvy3AockWQfQ/Ty4a1/KhcARA8vmIR9bmiSPgWkuDLXbp6q+leQW4DTg8u7nF6vqR3b5dLffDmzvb0syzENLE1VVN8LOr9++Y2Cv7JquBC5Osv9gn3AXqKbFbsM/yUXAqcBBwHVJHqiqY4AzgI8m+W3gIeD0Na1Uml4rOQZ2NnDumOuTfsRuw7+qzgLOWqL9LuAn16IoaY5dCHxkoG0jHgPTmA0720fSk/7hGFg36t/lMTB3gWpaeHkHaZWq6lvA4jEw2M0xMGkaGP7SCiS5KMk2ertqrktyR7fqDOAdSe4G3tH9LU0td/tIK+AxMM0LR/6S1CDDX5IaZPhLUoMMf0lqkOEvSQ0y/CWpQYa/JDXI8JekBhn+ktQgw1+SGmT4S1KDDH9JapDhL0kNMvwlqUGGvyQ1yPCXpAb5ZS6rsOmcayddgiQNxZG/JDXI8JekBhn+ktQgw1+SGmT4S1KDDH9JapDhL0kNMvwlqUGGvyQ1yPCXpAZ5eQdJGrFhL/2ycP4pI65k1xz5S1KDDH9JatCqd/skWQAe6RaAd1bVX6x2u5KktTOqff5vqKrbR7QtSdIaG8sB3yTrgfUDzRvH8djSuPgpWLNkVOF/RZIANwK/UVXbB9afDZw7oseSppmfgjUTRnHAd3NVvRR4GRDg4iVucyFwxMCyeQSPLc2UJOuTbOpf8FOwJmDVI/+q2tr9fDTJB4FPLXGb7cBOnwZ6HxSkueOn4Cnl167ubFUj/yT7JNm3+z3AG4FbRlGYNIP8FKyZsdqR/4HA1UnWAeuAO4FfX3VV0gzyU7BmyarCv6q+Chw/olqkmZVkH2DPqvqOn4I1C7y2jzQafgrWTDH8pRHwU7Bmjdf2kaQGGf6S1CDDX5IaZPhLUoMMf0lqkOEvSQ0y/CWpQYa/JDXI8JekBhn+ktQgw1+SGuS1fSRpSgzzhTML558y1GM58pekBhn+ktQgw1+SGmT4S1KDDH9JapDhL0kNcqqnJmqYqW0w/PQ2ST2Gv6SZMuyAQTtzt48kNcjwl6QGGf6S1CDDX5IaZPhLUoMMf0lqkOEvSQ2a2nn+47yu9bzyBKr54P+j1oIjf0lqkOEvSQ0y/CWpQYa/JDVo1eGf5Ogkn0tyd/fzqFEUJs0a+4JmyShG/pcCl1TV0cAlwGUj2KY0i+wLmhmrmuqZ5ADgBOCVXdOVwMVJ9q+q+/tutx5YP3D3wwG2bdu25LZ/+J1vrriehYWFFd9nNYapcRaM83kc9jncVY19r6d1Q214SNPWF2D8/WFc5rXfDWvovlBVQy/AicAdA213AicMtJ0HlIvLGJeTV/Pati+4zNGyZF8Y10leFwIfGWj7CeAzwM8CXxtTHdNmI3ADsBlYetg3/0b9HKwDngvcPIJtrYWl+sLT6fWHLwNPjPjxpv01Zn2r81T1PWVfWG34bwUOSbKuqp5Isg44uGv/B1W1Hdje35Zk8devVdXCKuuYSX3PwTafg5E+B/eMaDsrMXRf6Ny9FkVN+2vM+lZnGfXtsi+s6oBvVX0LuAU4rWs6Dfhi/z5OqQX2Bc2aUez2OQP4aJLfBh4CTh/BNqVZZF/QzFh1+FfVXcBPjqAWaabZFzRLJnmG73bgd1h6/2crfA58DtbatD+/1rc6Q9eXbvqZJKkhXttHkhpk+EtSgwx/SWqQ4S9JDTL8JalBY/8C9yQbgEO7P7dW1QPjrkGad/Yz7c7YRv5JjkzyGeArwBXd8pUkn2ntSy+SbEhyXLdsmHQ9k5JkfXeJY43ItPezJK/s+33fJB9Lck+Sq5McOMna+rXQR8e52+e/Af8V2FBVx1TVMcAG4MPdurk37R1zHJI8J8l/SfIwvasQ3pfku13b/pOubw5Mez97f9/v7wUeBn4BuAu4aCIV9ZmFPjqqN9Bxhv+GqrqiqnYsNlTVjqq6HHj2GOuYpGnvmONwOfBVYFNVPbOq9qF3OeMt3TqtzrT3s/T9fjLwb6rq9qr6TeBFE6qp3yz00ZG8gY4z/B9Mclr6rkGanl9iek+dHrVp75jjsKmq3tu/D7qqvl1V7wE2Ta6suTHt/ezHkrwwyYuAqqrH+9aN+rsMhjELfXQkb6DjDP9fAd5K78V5W5LbgAeAt3TrWjDtHXMcHknyU4ONSX4aeHQC9cybwX52O/Ag09PPngH8OXAtsD7JIQBJngXseKo7jsks9NGRvIGO/do+3X7dQ4FnAo8Ad3dfcDH3un2GlwLHA/d1zYfQuw78mVX1pUnVNi5JTgI+BvwAuLdr3gTsBbypqm6aUGlzZaCfbamqrbu5y1j1zUb6IU9+4chBVbVlclXNRh9NskDvjXLxDerkqrqvewO9vqpOWNZ2xhX+SV4HfBT4Or3rnF8FfB84APjVqvr0WAqZAn0dE3rT8Jr6wo9uVHUicFjX9DXg8+VVBlctyeH0wuufdk3bgb2B/wS8q6oem1RtMP31LZrFPprkGcCBy30DHWf4f5Fe6D8buAZ4TVV9NskLgY9X1fFjKWQKOAdbayXJ/wY+RG/Xyi8DzwEuAd4HPFpVb59geVNf36JZ6KOrrXGs4b8Y8EkWqmrTUuvmWZIjgf8MnEDvExD0vuf1C8AZVfXlSdU2Lt0L9v30Rv1/WlWX9K27uqpeP7Hi5kCSW6vqpX1//01VvTzJHsCXqmqi0xVnoL6p76OjqnGcB3yrO0jxU8A+3b5fkhxN71vmWzAL08jW2mX0DkBeCrw2yTVJFs80/4nJlTU3ftiFA0lOpDuI3s1eefyp7jgm017fLPTR0dRYVWNZgJ+n1+nvB/4J8D+B2+nt8zttXHVMcgHuGmbdPC3ArX2/h95H/v9B74DvFydd36wvwCldH7ttsa917QcCf2h9u61v6vvoqGoc27V9qurPgP0W/07yf4DjgG1V9c1x1TFhDyY5Dfij6v6nuoOf/4LpmUa21p6++Ev3HLw9yQfoTf3ba2JVzYmqurabsfI8ejPpvtu1fxN420SLY/rrYzb66Ehq9Gscx2gWppGttSTXAu+vqr8aaH8fcE5VeaVZTcws9NFR1Wj4T8AsTiMblST70Rv0P7TEuhdV1Z0TKEvaySz00dXWaPhPiSS3VdWxk65jknwONM1m4fW5khrHfj3/lnWnYy+5it7R+rn3FM8BNPIcaHrNQh8dVY2O/McoyQ5ggZ0vzLTokKp6+hLtc8XnQNNsFl6fo6rRkf94LQCbq+q+wRVJpuraK2toAZ8DTa8Fpv/1ucAIanRmxXhdDRy+i3XXjLOQCfI50DSbhdfnSGp0t48kNciRvyQ1yPCXpAYZ/pLUIMNfkhr0/wH/YuWrwUWBxAAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 2 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "drug = pd.DataFrame(dict(iq=(101,100,102,104,102,97,105,105,98,101,100,123,105,103,100,95,102,106,\n",
    "        109,102,82,102,100,102,102,101,102,102,103,103,97,97,103,101,97,104,\n",
    "        96,103,124,101,101,100,101,101,104,100,101),\n",
    "                         group='drug'))\n",
    "placebo = pd.DataFrame(dict(iq=(99,101,100,101,102,100,97,101,104,101,102,102,100,105,88,101,100,\n",
    "           104,100,100,100,101,102,103,97,101,101,100,101,99,101,100,100,\n",
    "           101,100,99,101,100,102,99,100,99),\n",
    "                            group='placebo'))\n",
    "\n",
    "trial_data = pd.concat([drug, placebo], ignore_index=True)\n",
    "trial_data.hist('iq', by='group')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Classical Statistical Hypothesis Testing for 2 groups with continuous outcome\n",
    "\n",
    "In [Statistical hypothesis testing](https://en.wikipedia.org/wiki/Statistical_hypothesis_testing), which is classical statistics for Data Science, you talk about [confidence intervals](https://en.wikipedia.org/wiki/Confidence_interval), the [null hypothesis](https://en.wikipedia.org/wiki/Null_hypothesis) (*nothing new happening*), [false positives and false negatives](https://en.wikipedia.org/wiki/False_positives_and_false_negatives), the [T-test](https://en.wikipedia.org/wiki/Student%27s_t-test), the [chi-squared test](https://en.wikipedia.org/wiki/Chi-squared_test), and [ANOVA](https://en.wikipedia.org/wiki/Analysis_of_variance), among other things.\n",
    "\n",
    "Bayesian analysis *supersedes* (is better than) statistical hypothesis testing. But there is still a *lot* of statistical hypothesis testing going on, and so it is good to learn something about it.\n",
    "\n",
    "**statistical hypothesis testing**, also called [confirmatory data analysis](https://en.wikipedia.org/wiki/Statistical_hypothesis_testing) is a framework for determining ***whether observed data deviates from what is expected***. \n",
    "\n",
    "A hypothesis is proposed for the statistical relationship between two data sets, and this is compared as an alternative to an idealized `null hypothesis` that proposes ***no relationship between two data sets**. Python's `scipy.stats` library contains an array of functions that make it easy to carry out hypothesis tests. Scipy.. I know, I know..\n",
    "\n",
    "</br >\n",
    "<center>\n",
    "<img src=\"https://denniegeorgesloth.weebly.com/uploads/4/0/8/7/40875523/4850818_orig.jpg\" width=\"300\" />\n",
    "Lazy Professor who hasn't officially introduced scipy yet!\n",
    "</center>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Statistical Hypothesis Testing in a nutshell\n",
    "\n",
    "If you wanted to test whether the statistics of the [placebo group](https://en.wikipedia.org/wiki/Clinical_trial#Placebo_groups) is different from the **drug group**, the **null hypothesis** states that ***there is no difference*** (nothing new).\n",
    "\n",
    "The purpose of a **hypothesis test** is to determine whether the null hypothesis is ***likely to be true*** given sample data. If there is little evidence against the null hypothesis given the data, you **accept** the null hypothesis. If the null hypothesis is unlikely given the data, you might **reject** the null hypothesis in favor of the alternative hypothesis: ***that something interesting/strange is going on***.\n",
    "\n",
    "Once you have the null and alternative hypothesis in hand, you choose a [significance level](https://en.wikipedia.org/wiki/Statistical_significance) (often denoted by the Greek letter $\\alpha$). The significance level is a probability threshold that determines when you ***reject*** the null hypothesis. After carrying out a test, if the probability of getting a result as strange as the one you observe is lower than the significance level, you reject the null hypothesis in favor of the alternative. If the probability is higher than $\\alpha$, then the null hypothesis is in effect and the result is ***not strange at all***. \n",
    "\n",
    "This probability of seeing a result as strange or more strange than the one observed is known as the [p-value](https://en.wikipedia.org/wiki/P-value). If the **p-value** is high, uncertainty in the experiment is high, and it will be difficult to conclude one way or the other (i.e. there is no difference). If the **p-value** is low, there is low probability to see a strange result, and so if we do see a strange result it is conclusive that the drug and placebo groups follow different distributions and thus that the drug group *has an effect*. \n",
    "\n",
    "####  What do I need to know?\n",
    "The **T-test** is a statistical test, based on the **p-value**, used to determine whether a numeric sampling differs significantly from the population, or whether ***two samples differ from one another***. That, in a nutshell, is traditional statistics for data science! "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Terminology\n",
    "\n",
    "A [confidence interval](https://en.wikipedia.org/wiki/Confidence_interval) is a **range** of sample values above and below a point estimate of a parameter (like the `mean`) that captures the true population parameter at some predetermined **confidence level**. \n",
    "\n",
    "For example, if you want to have a 95% chance of capturing the *true* population mean, you'd set your confidence level to 95%. Confidence levels are related to the size of your sampling, and the standard deviation of the *true* population.\n",
    "\n",
    "Sometimes we also say the *confidence level is 95%*.\n",
    "\n",
    "The [significance level](https://en.wikipedia.org/wiki/Statistical_significance) (denoted by $\\alpha$) is 1 - the confidence interval bound. So, a significance level of 0.05 corresponds to a confidence level of 95%. \n",
    "\n",
    "You calculate a confidence interval by taking a point estimate of some parameter (e.g. the mean) from a sample with size $n$, for example using MOM or MLE methods, and then adding and subtracting a [margin of error](https://en.wikipedia.org/wiki/Margin_of_error) to create a **range**. **Margin of error** is based on your desired confidence level, the spread of the population, and the size of your sample. \n",
    "\n",
    "This comes from the ***desire to obtain some kind of probability estimate on point estimates of model parameters***.\n",
    "\n",
    "The way you calculate the margin of error depends on whether you know the spread of the population or not. If you know the standard deviation $\\sigma$ of the population (a measure of spread), the margin of error is equal to:\n",
    "\n",
    "$$ z ∗ \\frac{\\sigma}{\\sqrt{n}}$$\n",
    "\n",
    "Where $z$ is a number known as the [z-critical value](https://en.wikipedia.org/wiki/Z-test). The **z-critical value** is the *number* of standard deviations you'd have to go from the mean of the distribution to capture the proportion of the data associated with the desired confidence level. \n",
    "\n",
    "For instance, we know that roughly 95% of the data in a *normal* (gaussian) distribution lies within 2 standard deviations of the mean, so we use 2 as the `z-critical value` for a 95% confidence interval. For all other distributions, you use the quantile function `stats.t.ppf` to compute $z$.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### T-test for comparing drug/placebo IQ outcomes\n",
    "\n",
    "[scipy.stats.ttest_ind](https://docs.scipy.org/doc/scipy-0.15.1/reference/generated/scipy.stats.ttest_ind.html) is\n",
    "a two-sided test for the null hypothesis that 2 independent samples have identical average (expected) values. This test assumes that ***the populations have identical variances***.\n",
    "\n",
    "The **T-test** measures whether the average (expected) value differs significantly across samples. If we observe a **large** p-value, for example larger than 0.1, then we cannot reject the null hypothesis of identical average scores, we have to say *there is nothing strange going on*. If the p-value is **smaller than the threshold**, then we reject the null hypothesis of equal averages (*there is something interesting going on*).\n",
    "\n",
    "Note that this test works on averages (expected) values, and that a **point estimate** does *not* capture the richness of all available statistics. In fact, it often returns bad results, *such as for this drug/placebo dataset*!\n",
    "\n",
    "Let's do the estimation for our chosen dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "47\n",
      "42\n",
      "101.91489361702128\n",
      "100.35714285714286\n",
      "101.17977528089888\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "Ttest_indResult(statistic=1.622190457290228, pvalue=0.10975381983712831)"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import scipy.stats as stats\n",
    "combined_iq = np.concatenate((drug.iq, placebo.iq))\n",
    "print(len(drug.iq))\n",
    "print(len(placebo.iq))\n",
    "print( drug.iq.mean() )\n",
    "print( placebo.iq.mean() )\n",
    "print( combined_iq.mean() )\n",
    "stats.ttest_ind(a= drug.iq,\n",
    "                b= placebo.iq,\n",
    "                equal_var=False) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The test yields a p-value of 0.10975, which means ***there is a 11% chance we'd see sample data this far apart statistically if the two groups tested are actually identical***. \n",
    "\n",
    "If we were using a 95% confidence level, we would **fail** to reject the null hypothesis, since the p-value is greater than the corresponding significance level of 0.05. And so we conclude that the drug is as effective as placebo at a 95% confidence level: there is nothing interesting going on. At a significance level of 0.05, drug outcome on IQ is statistically **nonsignificant**.\n",
    "\n",
    "The test result shows the test statistic is equal to 1.622. This test statistic tells us how much the sampled mean deviates from the null hypothesis. If the t-statistic lies outside the quantiles of the t-distribution corresponding to our confidence level and degrees of freedom, we need to reject the null hypothesis. We get the quantiles of the [Student-T](https://en.wikipedia.org/wiki/Student%27s_t-distribution) distribution using `stats.t.ppf`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "-1.6819523559426006"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "stats.t.ppf(q=0.05,  # Quantile to check\n",
    "            df=42)  # Degrees of freedom"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we see that our test statistic of 1.62 is ***within the quantile of the t-distribution (1.68)***, so we should accept the null hypothesis.\n",
    "\n",
    "When we do Bayesian analysis on the same dataset, we will see different results than classical hypothesis testing! \n",
    "\n",
    "I ***do not like*** classical hypothesis testing! The math is shaky, and results are dubious! But if you interview for a big pharma company that still does classical statistical inference, maybe you don't say this to them *before they hire you*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References and Resources\n",
    "\n",
    "- Goodman, S. N. (1999). Toward evidence-based medical statistics. 1: The P value fallacy. Annals of Internal Medicine, 130(12), 995–1004. http://doi.org/10.7326/0003-4819-130-12-199906150-00008\n",
    "- Johnson, D. (1999). The insignificance of statistical significance testing. Journal of Wildlife Management, 63(3), 763–772.\n",
    "- Gelman, A., Carlin, J. B., Stern, H. S., Dunson, D. B., Vehtari, A., & Rubin, D. B. (2013). Bayesian Data Analysis, Third Edition. CRC Press.\n",
    "-  Norvig, Peter. 2009. [The Unreasonable Effectiveness of Data](http://static.googleusercontent.com/media/research.google.com/en//pubs/archive/35179.pdf).\n",
    "- Salvatier, J, Wiecki TV, and Fonnesbeck C. (2016) Probabilistic programming in Python using PyMC3. *PeerJ Computer Science* 2:e55 <https://doi.org/10.7717/peerj-cs.55>\n",
    "- Cronin, Beau. \"Why Probabilistic Programming Matters.\" 24 Mar 2013. Google, Online Posting to Google . Web. 24 Mar. 2013. <https://plus.google.com/u/0/107971134877020469960/posts/KpeRdJKR6Z1>."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
