{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "<div style=\"text-align: right\">INFO 6105 Data Sci Engineering Methods and Tools, Week 3 Lecture 2</div>\n",
    "<div style=\"text-align: right\">Dino Konstantopoulos, 18 September 2019, with material from Cam Davidson-Pilon</div>\n",
    "\n",
    "At the end of this lecture, you should a good understanding of how to estimate probabilities for outcomes of different frequency and sample spaces, and how to use Bayes' theorem to answer typical interview questions involving probabilities. You are ready for an introduction to sport analytics.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "* * * \n",
    "# 5. Introduction to Probability Distributions\n",
    "\n",
    "<br />\n",
    "<center>\n",
    "<img src=\"http://statistics.wdfiles.com/local--files/ch6/binomial.png\" width=\"400\" />\n",
    "Binomial\n",
    "</center>\n",
    "\n",
    "So far, we have made the assumption that every outcome in a sample space (coin toss, die face, balls in urn) is **equally likely**. \n",
    "\n",
    "In real life however, we often get outcomes that are **not** equiprobable. For example, the probability of picking a royal in a deck of cards is not the same as the probability of picking a number. The probability of a child being a girl is not exactly 1/2, and the probability is slightly different for a second child. Give a read to this data science [article](http://people.kzoo.edu/barth/math105/moreboys.pdf) that gives the following *counts* for **two-child** families in Denmark, where `GB` means a family where the first child is a girl and the second a boy:\n",
    "\n",
    "    GG: 121801    GB: 126840\n",
    "    BG: 127123    BB: 135138\n",
    "    \n",
    "That's the number of two-baby births per couple in a year, distribited along the GG, GB, BG, and BB axes.\n",
    "\n",
    "Because of unequiprobably outcomes (not all urns yield Blue/Red/White balls with equiprobable chances), we need to introduce three more definitions:\n",
    "\n",
    "* [Frequency](https://en.wikipedia.org/wiki/Frequency_%28statistics%29): a number describing **how often** an outcome occurs. Can be a count like 121801, or a ratio like 0.515.\n",
    "\n",
    "* [Distribution](http://mathworld.wolfram.com/StatisticalDistribution.html): A **mapping** from outcome to frequency for each possible outcome in a sample space. \n",
    "\n",
    "* [Probability Distribution](https://en.wikipedia.org/wiki/Probability_distribution): The distribution above, which has been ***normalized*** so that the sum of the frequencies is 1.\n",
    "\n",
    "What is a distribution as defined above, in terms of the python container objects we covered in class?\n",
    "\n",
    "**It's a dictionary structure!!**\n",
    "\n",
    "Here is an example of a popular probability distribution:\n",
    "* The **binomial distribution** is frequently used to model the number of successes in a sample of size n drawn **with replacement** from a population of size N. It is parametrized by p and is the discrete probability distribution of the number of `yes`es in a sequence of n independent experiments *with replacement*, each asking a `yes/no` question, and each with its own boolean-valued outcome: a random variable containing a single bit of information: `yes` (with probability p) or `no` (with probability q = 1 − p)\n",
    "* A single `yes/no` experiment is also called a *Bernoulli trial* or *Bernoulli experiment* and a sequence of outcomes is called a *Bernoulli process*\n",
    "* For a single trial, i.e., n = 1, the binomial distribution is a **Bernoulli distribution**. \n",
    "\n",
    "So now we need to modify our *awesome* probability counting `p()` function to take this *miserable* fact into account.\n",
    "\n",
    "</p>\n",
    "<center>\n",
    "    <img src=\"ipynb.images/miserable.png\" width=200 />\n",
    "</center>\n",
    "\n",
    "But first, let's plot a few binomial distributions. This is an *unofficial* introduction to `numpy`'s awesome `matplotlib` package. Also, we use the `scipy` package to import the `binom` function. \n",
    "\n",
    "The probability of getting exactly k successes in n trials is given by the probability mass function:\n",
    "\n",
    "</p>\n",
    "<center>\n",
    "    <img src=\"ipynb.images/binom.png\" width=500 />\n",
    "</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# first, let's plot a few binomial distributions\n",
    "import numpy as np\n",
    "from scipy.stats import binom\n",
    "from matplotlib import pyplot as plt\n",
    "%matplotlib inline\n",
    "\n",
    "n_values = [20, 25, 40]\n",
    "p_values = [0.5, 0.5, 0.5]\n",
    "x = np.arange(0, 50)\n",
    "\n",
    "fig, ax = plt.subplots(figsize=(5, 3.75))\n",
    "\n",
    "for (n, p) in zip(n_values, p_values):\n",
    "    # create a binomial distribution\n",
    "    dist = binom(n, p)\n",
    "\n",
    "    plt.plot(x, dist.pmf(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ok, so let's define `ProbDist`!\n",
    "\n",
    "We define `ProbDist` to take the same kinds of arguments that dict does: either a mapping or a set of (key, val) pairs, and/or optional keyword arguments (because each ball in the urn will have its own probability of being picked, now!). \n",
    "\n",
    "Let's define a Python ***class***, instead of a Python function/lambda. We'll we define constructor `__init__()`. We assume `self` (`this` in Python) is composed of a set:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"display:none;\">\n",
    "class ProbDist(dict):\n",
    "    \"\"\"A Probability Distribution; an {outcome: probability} mapping.\"\"\"\n",
    "    def __init__(self, mapping=(), **kwargs):\n",
    "        self.update(mapping, **kwargs)\n",
    "        # Make probabilities sum to 1.0; assert no negative probabilities\n",
    "        total = sum(self.values())\n",
    "        for outcome in self:\n",
    "            self[outcome] = self[outcome] / total\n",
    "            assert self[outcome] >= 0\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "class ProbDist(dict):\n",
    "    \"\"\"A Probability Distribution; an {outcome: probability} mapping.\"\"\"\n",
    "    def __init__(self):\n",
    "        # Make probabilities sum to 1.0; assert no negative probabilities\n",
    "        total = sum(self.values())\n",
    "        for outcome in self:\n",
    "            self[outcome] = self[outcome] / total\n",
    "            assert self[outcome] >= 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "We now modify the functions `p` and `such_that` to accept either a sample space as we had previously, ***or a probability distribution as the second argument `space`***. If we have a probability distribution, instead of *counting* each possible outcome equiprobably and thus just summing up `1`s, we need to sum up the different discrete probabilities of each possible outcome: `sum(space[o] for o in space if o in event)`. \n",
    "\n",
    "We also need to modify `such_that()`, which is the set of all outcomes of our sample space for which the predicate (first) argument is `True`, so that its second argument can be a `ProbDist`.\n",
    "\n",
    "We introduce Python's `isinstance` operator, which tells us if a python **object** is of a specific **class**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "def p(event, space): \n",
    "    \"\"\"The probability of an event, given a sample space of outcomes. \n",
    "    event: a collection of outcomes, or a predicate that is true of outcomes in the event. \n",
    "    space: a set of outcomes or a probability distribution of {outcome: frequency} pairs.\"\"\"\n",
    "    \n",
    "    # if event is a predicate it, \"unroll\" it as a collection \n",
    "    if is_predicate(event):\n",
    "        event = such_that(event, space)\n",
    "        \n",
    "    # if space is not an equiprobably collection (a simple set), \n",
    "    # but a probability distribution instead (a dictionary set),\n",
    "    # then add (union) the probabilities for all favorable outcomes\n",
    "    if isinstance(space, ProbDist):\n",
    "        return sum(space[o] for o in space if o in event)\n",
    "    \n",
    "    # simplest case: what we played with in our previous lesson\n",
    "    else:\n",
    "        return Fraction(len(event & space), len(space))\n",
    "\n",
    "is_predicate = callable\n",
    "\n",
    "# Here we either return a simple collection in the case of equiprobable outcomes, or a dictionary collection in the\n",
    "# case of non-equiprobably outcomes\n",
    "def such_that(predicate, space): \n",
    "    \"\"\"The outcomes in the sample pace for which the predicate is true.\n",
    "    If space is a set, return a subset {outcome,...} with outcomes where predicate(element) is true;\n",
    "    if space is a ProbDist, return a ProbDist {outcome: frequency,...} with outcomes where predicate(element) is true.\"\"\"\n",
    "    if isinstance(space, ProbDist):\n",
    "        return ProbDist({o:space[o] for o in space if predicate(o)})\n",
    "    else:\n",
    "        return {o for o in space if predicate(o)}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "We used a ***set comprehension***.\n",
    "\n",
    "Uhhhh... What do we use for sets again in python?\n",
    "\n",
    "`{}`\n",
    "\n",
    "To note:\n",
    "* We are using a python *set* because we care about dictionaries, and dictionary keys are unique\n",
    "* You can also think in terms of JSON objects\n",
    "\n",
    "And now we can finally ***take on the Danes***!\n",
    "<br />\n",
    "<center>\n",
    "    <img src=\"ipynb.images/Danes.png\" width=300 />\n",
    "</center>\n",
    "\n",
    "Here is the probability distribution for Danish two-child families as a dictionary describing the probability of each possible outcome. It's a **set**, like our urn balls from last lecture, but now it's a ***set of key-value pairs*** instead (a dictionary) because each \"ball\" (2-child family) has a different probability of being picked (realized)!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [],
   "source": [
    "DK = ProbDist(GG=121801, GB=126840,\n",
    "              BG=127123, BB=135138)\n",
    "DK"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "Let's write some useful predicates (lambdas). You write a predicate for each possible yes/no question on something. That is how you convert your thinking into python objects. This is a ***very important step*** in your education as a data scientists/programmer. We started with this last lecture, where you wrote predicates for balls being red, white, or blue. Remember?\n",
    "\n",
    "```python\n",
    "def first_girl(outcome):  return outcome[0] == 'G'\n",
    "def first_boy(outcome):   return outcome[0] == 'B'\n",
    "def second_girl(outcome): return outcome[1] == 'G'\n",
    "def second_boy(outcome):  return outcome[1] == 'B'\n",
    "def two_girls(outcome):   return outcome    == 'GG'\n",
    "```\n",
    "Using these predicates, answer the following questions:\n",
    "\n",
    "* What's the probability for a girl, and is it *higher* or *lower* for a second girl?\n",
    "* Is the sex of the second child *more likely* or *less likely* to be the same as the first child?\n",
    "\n",
    "*Hint:* You will leverage `p(first_girl, DK)`, `p(second_girl, DK)`, `p(second_girl, such_that(first_girl, DK))`, and `p(second_girl, such_that(first_boy, DK))`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above says that the probability of a girl is somewhere between 48% and 49%, but that it is *slightly different* between the first or second child.\n",
    "\n",
    "Now answer the question as to whether the sex of the second child is *more likely* or *less likely* to be the same as the first child, by evaluating first:\n",
    "\n",
    "- The probability of a second girl given that the first child was a girl (a joint probability)\n",
    "- The probability of a second girl given that the first child was a boy (a joint probability)\n",
    "- The probability of a second boy given that the first child was a boy (a joint probability)\n",
    "- The probability of a second boy given that the first child was a girl (a joint probability)\n",
    "\n",
    "The average of the first two probabilities above represents the probability of a second girl, a [**marginal probability**](https://en.wikipedia.org/wiki/Marginal_distribution) in our problem.\n",
    "\n",
    "The avergage of the last two probabilities above represents the probability of a second boy, a **marginal probability** in our problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above says that the ***sex of the second child is more likely to be the same as the first child***, by about 1/2 a percentage point."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "We find that the probability of a girl is somewhere between 48% and 49%, but slightly different between the first or second child, and that the sex of the second child is more likely to be the same as the first child, by about 1/2 a percentage point.\n",
    "\n",
    "We've now began to work with **probability distributions**, which is nothing else than ***dictionaries***, where each key (outcome) had a different probability of occurring (value)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "# 6. M&Ms and Bayes\n",
    "<br />\n",
    "<center>\n",
    "<img src=\"https://upload.wikimedia.org/wikipedia/en/9/97/M%26M_spokescandies.jpeg\" />\n",
    "</center>\n",
    "\n",
    "Here's another classic urn problem (or \"bag\" problem) [from](http://allendowney.blogspot.com/2011/10/my-favorite-bayess-theorem-problems.html) prolific Python/Probability author [Allen Downey ](http://allendowney.blogspot.com/), which also happens to be a classic interview question:\n",
    "\n",
    "> The blue M&M was introduced in 1995.  Before then, the color mix in a bag of plain M&Ms was (30% Brown, 20% Yellow, 20% Red, 10% Green, 10% Orange, 10% Tan).  Afterward it was (24% Blue , 20% Green, 16% Orange, 14% Yellow, 13% Red, 13% Brown). \n",
    "A friend of mine has two bags of M&Ms, and he tells me that one is from 1994 and one from 1996.  He won't tell me which is which, but he gives me one M&M from each bag.  One is yellow and one is green.  What is the probability that the yellow M&M came from the 1994 bag? Well, the old M&M bags' yellow count was higher, so it must be higher, right? But how to count?\n",
    "\n",
    "To solve this problem, we'll first represent probability distributions for each bag: `bag94` and `bag96`, by using `ProbDist` and passing in dictionaries for each year:\n",
    "```python\n",
    "bag94 = ProbDist(brown=30, yellow=20, red=20, green=10, orange=10, tan=10)\n",
    "bag96 = ProbDist(...)  #fill this in, please\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "Next, define `MM` as the *joint* distribution 94-96&mdash;the sample space for picking *one* M&M from *each* bag. The outcome `'yellow green'` means that a yellow M&M was selected from the 1994 bag and a green one from the 1996 bag. This is very similar to our `cross` function from last lecture, except now we're working with python *dictionaries*.\n",
    "\n",
    "```python\n",
    "def joint(A, B, sep=''):\n",
    "    \"\"\"The joint distribution of two independent probability distributions. \n",
    "    Result is all entries of the form {a+sep+b: P(a)*P(b)}\"\"\"\n",
    "    return ProbDist({a + sep + b: A[a] * B[b]\n",
    "                    for ...\n",
    "                    for ...})\n",
    "\n",
    "MM = joint(bag94, bag96, ' ')\n",
    "MM\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "Let's look at the \"One is yellow and one is green\" part:\n",
    "\n",
    "```python\n",
    "def yellow_and_green(outcome): return 'yellow' in outcome and 'green' in outcome\n",
    "\n",
    "such_that(...) # fill this in\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "Now we can answer the question: given that we got a **yellow** and a **green** (but don't know which comes from which bag), what is the probability that the ***yellow came from the 1994 bag***?\n",
    "\n",
    "```python\n",
    "def yellow94(outcome): return ...\n",
    "\n",
    "p(yellow94, such_that(...)) # fill this in\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "So there is a 74% chance that the yellow comes from the 1994 bag. We were *right* about our hunch :-)\n",
    "\n",
    "Answering this question was straightforward: just like all the other probability problems, we simply create a sample space, and use `p` to pick out the probability of the event in question, given what we know about the outcome. This is the 'mechanistic' way of obtaining our answer.\n",
    "\n",
    "We can *also* solve it using *Bayes' Theorem*, and this is as good as any's introduction to **naive Bayes theory**: We are asked about the probability of an event (M&M94 --> M&M96) *given the evidence* (M&M94 is yellow, M&M96 green), which is not immediately available. However the probability of the evidence, given the event (***the inverse!***) is readily available!  \n",
    "\n",
    "Before we see the colors of the M&Ms, there are two hypotheses, `A` and `B`, both with equal probability:\n",
    "\n",
    "    A: first M&M from 94 bag, second from 96 bag\n",
    "    B: first M&M from 96 bag, second from 94 bag\n",
    "    P(A) = P(B) = 0.5\n",
    "    \n",
    "Then we get some evidence:\n",
    "    \n",
    "    E: first M&M yellow, second green\n",
    "    \n",
    "We want to know the probability of hypothesis `A`, given the evidence:\n",
    "    \n",
    "    P(A | E)\n",
    "    \n",
    "That's not easy to calculate (except by enumerating the sample space with python and a laptop, which is what we did above). But Bayes Theorem says:\n",
    "    \n",
    "    P(A | E) = P(E | A) * P(A) / P(E)\n",
    "    \n",
    "The quantities on the *right-hand-side* are easier to calculate:\n",
    "    \n",
    "    P(E | A) = 20/100 * 20/100 = 0.04\n",
    "    P(E | B) = 10/100 * 14/100 = 0.014\n",
    "    P(A)     = 0.5\n",
    "    P(B)     = 0.5\n",
    "    P(E)     = P(E | A) * P(A) + P(E | B) * P(B) \n",
    "             = 0.04     * 0.5  + 0.014    * 0.5   =   0.027\n",
    "             \n",
    "Where did the probability of the evidence P(E) formula come from?\n",
    "\n",
    "There are two possibilities of getting the evidence: A and B, a *union* and so we sum their probabilities. The joint probability of the evidence *and* case A is a succession or *intersection*, so it must be a product of their probabilities: P(E|A).P(A). Likewise for the case B: P(E|B).P(B) \n",
    "    \n",
    "And so we can get a final answer:\n",
    "    \n",
    "    P(A | E) = P(E | A) * P(A) / P(E) \n",
    "             = 0.04     * 0.5  / 0.027 \n",
    "             = 0.7407407407\n",
    "             \n",
    "Bayes Theorem allows you to do less calculation at the cost of more algebra; that is a great trade-off if you are working with pencil and paper (like in ***interview situations!!!***). Enumerating the state space allows you to do less algebra at the cost of more calculation; often a good trade-off if you have a computer. \n",
    "\n",
    "Regardless of the approach you use, it is important to understand Bayes theorem and how it works."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Conclusion¶\n",
    "We've built a framework for estimating **probabilities** that will come in handy when you are asked to build **data models**. \n",
    "\n",
    "We played with Bayes' theorem, a pillar of data science, and we started learning how to answer typical Data Science interview questions, which you should *always* answer in python because you *will* run out of space on the whiteboard if you use squiggly brackets in other languages! \n",
    "\n",
    "Next week, we move on to statistical modeling and inference. **Modeling** (one `l` or 2 `l`s? See [here](https://www.grammarly.com/blog/modeling-or-modelling/)) happens when data is scarce and precious and hard to obtain, for example in social sciences and settings where it is difficult to conduct large-scale controlled experiments. With small data it is important to quantify uncertainty and that’s precisely what Bayesian approaches are good at. **Inference** refers to how you learn parameters of your model (Markov Chain Monte Carlo, or MCMC, albeit computationally expensive, is one of the most important methods for statistical inference), which is especially important with Bayesian Machine Learning, where we can actually inquire with Machines why this or that action was undertaken.\n",
    "\n",
    "*Alexa, why did you lower the temperature in the bedroom?* **Because your wife told me that whenever you start snoring, John, colder tempreatures make you bundle up under the cover and you snore less**. \n",
    "\n",
    "<br />\n",
    "<center>\n",
    "    <img src=\"ipynb.images/echo.jpg\" width=200 />\n",
    "</center>\n",
    "\n",
    "Best advice for interviews: Be explicit about what the problem says, have the interviewer verify the working hypotheses, be methodical about defining the sample space, be careful in counting the number of outcomes in the numerator and denominator, and finally use Bayes' theorem (or 1 minus the negation) whenever possible because you will be doing calculations by hand on a whiteboard!\n",
    "\n",
    "<br />\n",
    "<center>\n",
    "    <img src=\"ipynb.images/jobinterview.jpg\" width=300 />\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Homework: Introduction to Data Science for Sports\n",
    "\n",
    "<br />\n",
    "<center>\n",
    "    <img src=\"ipynb.images/f1races.png\" width=800 />\n",
    "</center>\n",
    "\n",
    "Question 1.1 (20 points) There are a number of F1 races coming up: \n",
    "- Singapore GP: Date: Sun, Sep 22, 8:10 AM\n",
    "- Russian GP: Date: Sun, Sep 29, 7:10 AM\n",
    "- Japanese GP: Date: Sun, Oct 13, 1:10 AM\n",
    "- Mexican GP Date: Sun, Oct 13, 1:10 AM\n",
    "\n",
    "The Singaporean Grand Prix this weekend and the Russian Grand Prix the weekend after, as you can see [here](https://www.formula1.com/en/racing/2019.html). \n",
    "\n",
    "The 2019 driver standings are given [here](https://www.formula1.com/en/results.html/2019/drivers.html). Given these standings (please do not use team standings given onthe same Web site, use driver standings), what is the Probability Distribution for each F1 driver to win the Singaporean Grand Prix? What is the Probability Distribution for each F1 driver to win *both* the Singaporean and Russian Grand Prix? What is the probability for Mercedes to win both races? What is the probability for Mercedes to win at least one race? Note that Mercedes, and each other racing team, has two drivers per race.\n",
    "\n",
    "Question 1.2 (30 points) If Mercedes wins the first race, what is the probability that Mercedes wins the next one? If Mercedes wins at least one of these two races, what is the probability Mercedes wins both races? How about Ferrari, Red Bull, and Renault?\n",
    "\n",
    "Question 1.3 (50 points) Mercedes wins one of these two races on a **rainy** day. What is the probability Mercedes wins both races, assuming races can be held on either rainy, sunny, cloudy, snowy or foggy days? Assume that rain, sun, clouds, snow, and fog are the *only possible weather conditions* on race tracks.\n",
    "\n",
    "You need to provide *proof* for your answers. `I think it's one in a million because Mercedes sucks and I like Ferrari a lot more` is not a good answer. Leverage the counting framework in this workbook!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hint: Use SingaporeanGrandPrix, or `SGP` to denote the Probability Distribution given by F1 driver wins. Write driver initials as keys and driver wins as values in a dictionary that you pass to our function `ProbDist`..\n",
    "\n",
    "### Question 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SGP = ProbDist(\n",
    "    LH = 284,\n",
    "    ...)\n",
    "SGP"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
