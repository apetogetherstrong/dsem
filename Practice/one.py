import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 
import seaborn as sns
import math 
sns.set_style('whitegrid') 
sns.set_context('talk')
n_points = 200
x = np.linspace(0, 2, n_points)
y = np.array([0] * int(n_points / 2) + list(x[:int(n_points / 2)])) * 2
plt.figure(figsize=(5, 2)) 
plt.plot(x, y, linewidth=2) 
plt.title('ridiculously simple data') 
plt.xlabel('a')
plt.ylabel('b')
plt.show()
import pdb;pdb.set_trace()

from keras.models import Sequential
from keras.layers.core import Dense, Activation 
import numpy as np
np.random.seed(0)
model = Sequential()
model.add(Dense(output_dim=1, input_dim=1, init="normal")) 
model.add(Activation("relu")) 
model.compile(loss='mean_squared_error', optimizer='sgd')
# print initial weigths
weights = model.layers[0].get_weights()
w0 = weights[0][0][0]
w1 = weights[1][0]
print('neural net initialized with weigths w0: {w0:.2f}, w1: {w1:.2f}'.format(**locals())) 
pdb.set_trace()
print("done with second step")